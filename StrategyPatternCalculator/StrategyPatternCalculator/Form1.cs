﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace StrategyPattern
{
    public partial class Form1 : Form
    {
        
        public Form1()
        {
            InitializeComponent();
        }

        //STRATEGY PATTERN

        //a pattern that executes a strategy on runtime. 
        //one of the benefits: extensible with less impact into your code.
        //if you want to add another strategy, just register to factory.

        //Strategies are Add, Subtract, Multiply and Divide
        private void BtnCompute_Click(object sender, EventArgs e)
        {
            if (cboOperator.Text == "- select -" || txtNum1.Text == "" || txtNum2.Text == "")
            {
                return;
            }

            //Instantiate factory and passing an argument into a method from factory class to make a new strategy.
            ICalculator calculator = new Factory().MakeOperation(cboOperator.Text);

            //set input
            calculator.num1        = double.Parse(txtNum1.Text);
            calculator.num2        = double.Parse(txtNum2.Text);
            
            //parse
            string result          = calculator.Compute().ToString();

            //display output
            MessageBox.Show(result);
        }

        class Factory
        {
            //You can use switch case statement either way to return an object
            IDictionary<string, ICalculator> container = new Dictionary<string, ICalculator>();

            public Factory()
            {
                //avoid data redundancy
                container.Clear();

                //Registering the Strateggies into container
                container.Add("+", new Add());
                container.Add("-", new Subtract());
                container.Add("*", new Multiply());
                container.Add("/", new Divide());
            }

            //Returning a Strategy base on the argument
            public ICalculator MakeOperation(string strOperator)
            {
                return container[strOperator];
            }
        }

        //using interface hides the concrete implementation of Calculator
        interface ICalculator
        {
            double num1 { get; set; }
            double num2 { get; set; }
            double Compute();
        }

        //abstract class cant be instantiate
        abstract class Operand
        {
            public double num1 { get; set; }
            public double num2 { get; set; }
        }

        //base class
        abstract class Calculator : Operand, ICalculator
        {
            public abstract double Compute();
        }

        //inheriting base class
        class Add : Calculator
        {
            public override double Compute()
            {
                return num1 + num2;
            }
        }

        //inheriting base class
        class Subtract : Calculator
        {
            public override double Compute()
            {
                return num1 - num2;
            }
        }

        //inheriting base class
        class Multiply : Calculator
        {
            public override double Compute()
            {
                return num1 * num2;
            }
        }

        //inheriting base class
        class Divide : Calculator
        {
            public override double Compute()
            {
                return num1 / num2;
            }
        }
    }
}
